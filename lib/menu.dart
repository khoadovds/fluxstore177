import 'dart:async';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import 'common/config.dart';
import 'common/constants.dart';
import 'common/tools.dart';
import 'generated/l10n.dart';
import 'models/app.dart';
import 'models/category/category.dart';
import 'models/category/category_model.dart';
import 'models/product/product.dart';
import 'models/user/user_model.dart';

class MenuBar extends StatefulWidget {
  final GlobalKey<NavigatorState> navigation;
  final StreamController<String> controllerRouteWeb;

  MenuBar({this.navigation, this.controllerRouteWeb});

  @override
  _MenuBarState createState() => _MenuBarState();
}

class _MenuBarState extends State<MenuBar> {
  Widget imageContainer(String link) {
    if (link.contains('http://') || link.contains('https://')) {
      return Image.network(
        link,
        fit: BoxFit.cover,
      );
    }
    return Image.asset(
      link,
      fit: BoxFit.cover,
    );
  }

  Widget drawerItem(item) {
    final isTablet = Tools.isTablet(MediaQuery.of(context));
    kLayoutWeb = (kIsWeb || isTablet);

    if (item['show'] == false) return Container();
    switch (item['type']) {
      case 'home':
        {
          return ListTile(
            leading: const Icon(
              Icons.shopping_basket,
              size: 20,
            ),
            title: Text(S.of(context).shop),
            onTap: () {
              if (kLayoutWeb) {
                widget.controllerRouteWeb.sink.add("/home-screen");
              } else {
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                } else {
                  Navigator.of(context).pushReplacementNamed("/home-screen");
                }
              }
            },
          );
        }
      case 'web':
        {
          return Column(
            children: [
              if (kLayoutWeb)
                ListTile(
                  leading: const Icon(
                    Icons.list,
                    size: 20,
                  ),
                  title: Text(S.of(context).category),
                  onTap: () {
                    widget.controllerRouteWeb.sink.add("/category");
                  },
                ),
              if (kLayoutWeb)
                ListTile(
                  leading: const Icon(
                    Icons.search,
                    size: 20,
                  ),
                  title: Text(S.of(context).search),
                  onTap: () {
                    widget.controllerRouteWeb.sink.add("/search");
                  },
                ),
              if (kLayoutWeb)
                ListTile(
                  leading: const Icon(Icons.settings, size: 20),
                  title: Text(S.of(context).settings),
                  onTap: () {
                    if (kLayoutWeb) {
                      widget.controllerRouteWeb.sink.add("/setting");
                    } else {
                      Navigator.of(context).pushNamed("/setting");
                    }
                  },
                )
            ],
          );
        }
      case 'blog':
        {
          return ListTile(
            leading: const Icon(FontAwesomeIcons.wordpress, size: 20),
            title: Text(S.of(context).blog),
            onTap: () {
              if (kLayoutWeb) {
                widget.controllerRouteWeb.sink.add("/blogs");
              } else {
                Navigator.of(context).pushNamed("/blogs");
              }
            },
          );
        }
      case 'login':
        {
          bool loggedIn = Provider.of<UserModel>(context).loggedIn;
          return ListTile(
            leading: const Icon(Icons.exit_to_app, size: 20),
            title: loggedIn
                ? Text(S.of(context).logout)
                : Text(S.of(context).login),
            onTap: () {
              if (loggedIn) {
                Provider.of<UserModel>(context, listen: false).logout();
              } else {
                if (kLayoutWeb) {
                  widget.controllerRouteWeb.sink.add("/login");
                } else {
                  Navigator.pushNamed(context, "/login");
                }
              }
            },
          );
        }
      case 'category':
        {
          return Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              ExpansionTile(
                initiallyExpanded: true,
                title: Text(
                  S.of(context).byCategory.toUpperCase(),
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).accentColor.withOpacity(0.5),
                  ),
                ),
                children: showCategories(),
              )
            ],
          );
        }
      default:
        return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    printLog("[MenuBar] build");
    Map<String, dynamic> drawer =
        Provider.of<AppModel>(context, listen: false).drawer ?? kDefaultDrawer;

    return SingleChildScrollView(
      key: drawer['key'] != null ? Key(drawer['key']) : null,
      child: Column(
        children: <Widget>[
          DrawerHeader(
            padding: const EdgeInsets.all(0),
            child: Stack(
              children: [
                if (drawer['background'] != null)
                  Container(
                    child: imageContainer(drawer['background']),
                  ),
                if (drawer['logo'] != null)
                  Align(
                    alignment: const Alignment(-0.8, 0.6),
                    child: Container(
                      height: 38,
                      child: imageContainer(drawer['logo']),
                    ),
                  )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child: Column(
              children: List.generate(drawer['items'].length, (index) {
                return drawerItem(drawer['items'][index]);
              }),
            ),
          )
        ],
      ),
    );
  }

  List showCategories() {
    final categories = Provider.of<CategoryModel>(context).categories;
    List<Widget> widgets = [];

    if (categories != null) {
      var list = categories.where((item) => item.parent == '0').toList();
      for (var index in list) {
        widgets.add(
          ExpansionTile(
            title: Padding(
              padding: const EdgeInsets.only(left: 0.0, top: 0),
              child: Text(
                index.name.toUpperCase(),
                style: const TextStyle(fontSize: 14),
              ),
            ),
            children: getChildren(categories, index),
          ),
        );
      }
    }
    return widgets;
  }

  List getChildren(List<Category> categories, Category category) {
    List<Widget> list = [];
    var children = categories.where((o) => o.parent == category.id).toList();

    list.add(
      ListTile(
        leading: Padding(
          child: Text(S.of(context).seeAll),
          padding: const EdgeInsets.only(left: 20),
        ),
        trailing: const Icon(
          Icons.arrow_forward_ios,
          size: 12,
        ),
        onTap: () {
          Product.showList(
              context: context, cateId: category.id, cateName: category.name);
        },
      ),
    );
    for (var i in children) {
      list.add(
        ListTile(
          leading: Padding(
            child: Text(i.name),
            padding: const EdgeInsets.only(left: 20),
          ),
          trailing: const Icon(
            Icons.arrow_forward_ios,
            size: 12,
          ),
          onTap: () {
            Product.showList(context: context, cateId: i.id, cateName: i.name);
          },
        ),
      );
    }
    return list;
  }
}
