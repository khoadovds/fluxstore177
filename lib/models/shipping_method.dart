import 'package:flutter/material.dart';

import '../common/constants.dart';
import '../models/cart/cart_model.dart';
import '../services/index.dart';

class ShippingMethodModel extends ChangeNotifier {
  final Services _service = Services();
  List<ShippingMethod> shippingMethods;
  bool isLoading = true;
  String message;

  Future<void> getShippingMethods(
      {CartModel cartModel, String token, String checkoutId}) async {
    try {
      isLoading = true;
      notifyListeners();
      shippingMethods = await _service.getShippingMethods(
          cartModel: cartModel, token: token, checkoutId: checkoutId);
      isLoading = false;
      message = null;
      notifyListeners();
    } catch (err) {
      isLoading = false;
      message = "⚠️ " + err.toString();
      notifyListeners();
    }
  }
}

class ShippingMethod {
  String id;
  String title;
  String description;
  double cost;
  double min_amount;
  String classCost;
  String methodId;
  String methodTitle;

  Map<String, dynamic> toJson() {
    return {"id": id, "title": title, "description": description, "cost": cost};
  }

  ShippingMethod.fromJson(Map<String, dynamic> parsedJson) {
    try {
      id = "${parsedJson["instance_id"]}";
      title = parsedJson["label"];
      //description = parsedJson["method_description"];
      methodId = parsedJson["method_id"];
      methodTitle = parsedJson["label"];
      cost = double.parse(parsedJson["cost"]);
//      if (parsedJson["settings"]["cost"] != null) {
//        cost = double.parse(parsedJson["settings"]["cost"]["value"]);
//      }
//      parsedJson["settings"]["min_amount"] != null
//          ? min_amount =
//              double.parse(parsedJson["settings"]["min_amount"]["value"])
//          : min_amount = null;
//      Map settings = parsedJson["settings"];
//      settings.keys.forEach((key) {
//        if (key is String && key.contains("class_cost_")) {
//          classCost = parsedJson["settings"][key]["value"];
//        }
//      });
    } catch (e) {
      printLog('error parsing Shipping method');
    }
  }

  ShippingMethod.fromMagentoJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["carrier_code"];
    methodId = parsedJson["method_code"];
    title = parsedJson["carrier_title"];
    description = parsedJson["method_title"];
    cost = parsedJson["amount"] != null
        ? double.parse("${parsedJson["amount"]}")
        : 0.0;
  }

  ShippingMethod.fromOpencartJson(Map<String, dynamic> parsedJson) {
    Map<String, dynamic> quote = parsedJson["quote"];
    if (quote["code"] == null &&
        quote.values.isNotEmpty &&
        quote.values.toList()[0] is Map) {
      quote = quote.values.toList()[0];
    }
    String title =
        quote["title"] != null ? quote["title"] : parsedJson["title"];
    id = quote["code"];
    this.title = title ?? id;
    description = title ?? "";
    cost = quote["cost"] != null ? double.parse("${quote["cost"]}") : 0.0;
  }

  ShippingMethod.fromShopifyJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["handle"];
    title = parsedJson["title"];
    description = parsedJson["title"];
    var price = parsedJson["priceV2"] ?? parsedJson["price"] ?? "0";
    cost = double.parse(price);
  }

  ShippingMethod.fromPrestaJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'].toString();
    title = parsedJson['name'];
    description = parsedJson['delay'];
    cost = double.parse('${parsedJson['shipping_external']}');
  }
}
