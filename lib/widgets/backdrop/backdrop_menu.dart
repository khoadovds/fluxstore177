import 'package:flutter/material.dart';
import '../../models/product/product_model.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/app.dart';
import '../../models/category/category.dart';
import '../../models/category/category_model.dart';
import '../../models/filter_attribute.dart';
import '../common/tree_view.dart';
import '../layout/layout_web.dart';
import 'category_item.dart';

class BackdropMenu extends StatefulWidget {
  final Function onFilter;
  final String categoryId;

  const BackdropMenu({
    Key key,
    this.onFilter,
    this.categoryId,
  }) : super(key: key);

  @override
  _BackdropMenuState createState() => _BackdropMenuState();
}

class _BackdropMenuState extends State<BackdropMenu> {
  double mixPrice = 0.0;
  double maxPrice = kMaxPriceFilter / 2;
  String categoryId = '-1';
  String currentSlug;
  int currentSelectedAttr = -1;

  @override
  void initState() {
    super.initState();
    categoryId = widget.categoryId;
  }

  @override
  Widget build(BuildContext context) {
    final category = Provider.of<CategoryModel>(context);
    final selectLayout = Provider.of<AppModel>(context).productListLayout;
    final currency = Provider.of<AppModel>(context).currency;
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    final filterAttr = Provider.of<FilterAttributeModel>(context);

    categoryId = Provider.of<ProductModel>(context).categoryId;

    Function _onFilter = (categoryId) => widget.onFilter(
          minPrice: mixPrice,
          maxPrice: maxPrice,
          categoryId: categoryId,
          attribute: currentSlug,
          currentSelectedTerms: filterAttr.lstCurrentSelectedTerms,
        );

    return ListenableProvider.value(
      value: category,
      child: Consumer<CategoryModel>(
        builder: (context, value, child) {
          if (value.isLoading) {
            printLog('Loading');
            return Center(child: Container(child: kLoadingWidget(context)));
          }

          if (value.categories != null) {
            printLog(value.categories.length);
            final categories =
                value.categories.where((item) => item.parent == '0').toList();

            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  kLayoutWeb
                      ? SizedBox(
                          height: 100,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              const SizedBox(width: 20),
                              GestureDetector(
                                child: const Icon(Icons.arrow_back_ios,
                                    size: 22, color: Colors.white70),
                                onTap: () {
                                  if (kLayoutWeb) {
                                    LayoutWebCustom.changeStateMenu(true);
                                  }
                                  Navigator.of(context).pop();
                                },
                              ),
                              const SizedBox(width: 20),
                              Text(
                                S.of(context).products,
                                style: const TextStyle(
                                  fontSize: 21,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white70,
                                ),
                              ),
                            ],
                          ),
                        )
                      : const SizedBox(),
                  const SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Text(
                      S.of(context).layout.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Colors.white70,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  Wrap(
                    children: <Widget>[
                      for (var item in kProductListLayout)
                        GestureDetector(
                          onTap: () =>
                              Provider.of<AppModel>(context, listen: false)
                                  .updateProductListLayout(item['layout']),
                          child: Container(
                            width: 40,
                            height: 40,
                            margin: const EdgeInsets.all(10.0),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Image.asset(
                                item['image'],
                                color: selectLayout == item['layout']
                                    ? Colors.white
                                    : Colors.black.withOpacity(0.2),
                              ),
                            ),
                            decoration: BoxDecoration(
                                color: selectLayout == item['layout']
                                    ? Colors.black.withOpacity(0.15)
                                    : Colors.black.withOpacity(0.05),
                                borderRadius: BorderRadius.circular(9.0)),
                          ),
                        )
                    ],
                  ),
                  const SizedBox(height: 40),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Text(
                      S.of(context).byPrice.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Colors.white70,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        Tools.getCurrecyFormatted(mixPrice, currencyRate,
                            currency: currency),
                        style:
                            const TextStyle(color: Colors.white, fontSize: 16),
                      ),
                      const Text(
                        " - ",
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                      Text(
                        Tools.getCurrecyFormatted(maxPrice, currencyRate,
                            currency: currency),
                        style:
                            const TextStyle(color: Colors.white, fontSize: 16),
                      )
                    ],
                  ),
                  SliderTheme(
                    data: const SliderThemeData(
                      activeTrackColor: Color(kSliderActiveColor),
                      inactiveTrackColor: Color(kSliderInactiveColor),
                      activeTickMarkColor: Colors.white70,
                      inactiveTickMarkColor: Colors.black,
                      overlayColor: Colors.black12,
                      thumbColor: Color(kSliderActiveColor),
                      showValueIndicator: ShowValueIndicator.always,
                    ),
                    child: RangeSlider(
                      min: 0.0,
                      max: kMaxPriceFilter,
                      divisions: kFilterDivision,
                      values: RangeValues(mixPrice, maxPrice),
                      onChanged: (RangeValues values) {
                        setState(() {
                          mixPrice = values.start;
                          maxPrice = values.end;
                        });
                      },
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, top: 30),
                    child: Text(
                      S.of(context).attributes.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Colors.white70,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  ListenableProvider.value(
                    value: filterAttr,
                    child: Consumer<FilterAttributeModel>(
                      builder: (context, value, child) {
                        if (value.lstProductAttribute != null) {
                          List<Widget> list = List.generate(
                            value.lstProductAttribute.length,
                            (index) {
                              return InkWell(
                                onTap: () {
                                  if (!value.isLoading) {
                                    currentSelectedAttr = index;

                                    currentSlug =
                                        value.lstProductAttribute[index].slug;
                                    value.getAttr(
                                        id: value
                                            .lstProductAttribute[index].id);
                                  }
                                },
                                child: Container(
                                  margin: const EdgeInsets.all(5.0),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 13.0, vertical: 10.0),
                                    child: Text(
                                      value.lstProductAttribute[index].name
                                          .toUpperCase(),
                                      style: currentSelectedAttr != -1
                                          ? currentSelectedAttr == index
                                              ? const TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  letterSpacing: 1.2,
                                                  fontSize: 13,
                                                )
                                              : TextStyle(
                                                  color: Colors.black
                                                      .withOpacity(0.3),
                                                  fontWeight: FontWeight.bold,
                                                  letterSpacing: 1.2,
                                                  fontSize: 13,
                                                )
                                          : TextStyle(
                                              color:
                                                  Colors.black.withOpacity(0.3),
                                              fontWeight: FontWeight.bold,
                                            ),
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                    color: currentSelectedAttr != -1
                                        ? currentSelectedAttr == index
                                            ? Colors.black.withOpacity(0.15)
                                            : Colors.black.withOpacity(0.05)
                                        : Colors.black.withOpacity(0.05),
                                    borderRadius: BorderRadius.circular(9.0),
                                  ),
                                ),
                              );
                            },
                          );

                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Container(
                                child: Wrap(
                                  children: [
                                    ...list,
                                  ],
                                ),
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 10),
                              ),
                              value.isLoading
                                  ? Center(
                                      child: Container(
                                        margin: const EdgeInsets.only(
                                          top: 10.0,
                                        ),
                                        width: 25.0,
                                        height: 25.0,
                                        child: const CircularProgressIndicator(
                                            strokeWidth: 2.0),
                                      ),
                                    )
                                  : Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 5),
                                      child: currentSelectedAttr == -1
                                          ? Container()
                                          : Wrap(
                                              children: List.generate(
                                                value.lstCurrentAttr.length,
                                                (index) {
                                                  return Container(
                                                    margin: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 5),
                                                    child: FilterChip(
                                                      label: Text(value
                                                          .lstCurrentAttr[index]
                                                          .name),
                                                      selected: value
                                                              .lstCurrentSelectedTerms[
                                                          index],
                                                      onSelected: (val) {
                                                        value.updateAttributeSelectedItem(index, val);
                                                      },
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                    ),
                            ],
                          );
                        }
                        return Container();
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, top: 30),
                    child: Text(
                      S.of(context).byCategory.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Colors.white70,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 20),
                    child: Container(
                      padding: const EdgeInsets.only(top: 15.0),
                      decoration: BoxDecoration(
                          color: Colors.black12,
                          borderRadius: BorderRadius.circular(3.0)),
                      child: TreeView(
                        parentList: [
                          for (var item in categories)
                            Parent(
                              parent: CategoryItem(
                                item,
                                hasChild:
                                    hasChildren(value.categories, item.id),
                                isSelected: item.id == categoryId,
                                onTap: (category) => _onFilter(category),
                              ),
                              childList: ChildList(
                                children: [
                                  Parent(
                                    parent: CategoryItem(
                                      item,
                                      isLast: true,
                                      isParent: true,
                                      isSelected: item.id == categoryId,
                                      onTap: (category) => _onFilter(category),
                                    ),
                                    childList: ChildList(
                                      children: const [],
                                    ),
                                  ),
                                  for (var category in getSubCategories(
                                      value.categories, item.id))
                                    Parent(
                                        parent: CategoryItem(
                                          category,
                                          isLast: true,
                                          isSelected: category.id == categoryId,
                                          onTap: (category) =>
                                              _onFilter(category),
                                        ),
                                        childList: ChildList(
                                          children: const [],
                                        ))
                                ],
                              ),
                            )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      children: [
                        Expanded(
                          child: ButtonTheme(
                            height: 44,
                            child: RaisedButton(
                              elevation: 0.0,
                              color: Colors.white70,
                              onPressed: () => _onFilter(categoryId),
                              child: Text(S.of(context).apply),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3.0),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(height: 70)
                ],
              ),
            );
          }

          return null;
        },
      ),
    );
  }

  bool hasChildren(categories, id) {
    return categories.where((o) => o.parent == id).toList().length > 0;
  }

  List<Category> getSubCategories(categories, id) {
    return categories.where((o) => o.parent == id).toList();
  }
}
