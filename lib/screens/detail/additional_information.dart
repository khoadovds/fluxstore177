import 'package:flutter/material.dart';

import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../widgets/common/tooltip.dart' as tool_tip;
import '../../models/product/product_attribute.dart';

class AdditionalInformation extends StatelessWidget {
  final List<ProductAttribute> listInfo;

  AdditionalInformation({@required this.listInfo});

  @override
  Widget build(BuildContext context) {
    final lengthInfo = listInfo.length;
    return Column(
        children: List.generate(listInfo.length, (index) {
          Color color;
          if (index.isEven && lengthInfo > 2) {
            color = Theme
                .of(context)
                .primaryColorLight;
          }
          return renderItem(
            attribute: listInfo[index],
            color: color,
          );
        }));
  }

  Widget renderItem({
    ProductAttribute attribute,
    Color color,
  }) {
    if (attribute == null) return const SizedBox();

    return Container(
      color: color,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 4,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 4.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    attribute.name.toUpperCase(),
                    style: const TextStyle(
                      fontSize: 13,
                      height: 1.4,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(width: 8),
            Expanded(
              flex: 6,
              child: attribute.name != "color"
                  ? Align(
                alignment: Alignment.centerLeft,
                child: Text(attribute.options.join(", "),
                    style: const TextStyle(
                      color: kGrey600,
                      fontSize: 14,
                    )),
              )
                  : Row(
                children: <Widget>[
                  for (var i = 0; i < attribute.options.length; i++)
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: tool_tip.Tooltip(
                        child: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            color: HexColor(
                              kNameToHex[attribute.options[i]
                                  .toString()
                                  .replaceAll(' ', "_")
                                  .toLowerCase()],
                            ),
                          ),
                        ),
                        message: attribute.options[i] != null
                            ? attribute.options[i]
                            : '',
                      ),
                    )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
